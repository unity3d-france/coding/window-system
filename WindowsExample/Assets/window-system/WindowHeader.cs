﻿// Copyright ©2019, Andre Plourde
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the “Software”), to deal in the Software without 
// restriction, including without limitation the rights to use, copy, modify, merge, publish, 
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom 
// the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or 
// substantial portions of the Software.
//
// The Software is provided “as is”, without warranty of any kind, express or implied, 
// including but not limited to the warranties of merchantability, fitness for a particular 
// purpose and noninfringement. In no event shall the authors or copyright holders X be 
// liable for any claim, damages or other liability, whether in an action of contract, tort or
// otherwise, arising from, out of or in connection with the software or the use or other 
// dealings in the Software.

using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// le component representant l'entete d'une fenetre, elle permet entre autre le draging 
/// de la fenetre.
/// </summary>
public class WindowHeader : MonoBehaviour, IBeginDragHandler, IDragHandler, IPointerDownHandler  {
    private RectTransform _RectTrans = null;
    private WindowSystem _windowSystem = null;

    private void Awake() {
        _RectTrans = GetComponentInParent<Window>().GetComponent<RectTransform>();
        _windowSystem = WindowSystem.Instance;
    }

    public void OnDrag(PointerEventData data) {
        _RectTrans.anchoredPosition += data.delta;
        Window w = GetComponentInParent<Window>();
        _windowSystem.BringToFront(w);
    }

    public void OnBeginDrag(PointerEventData eventData) {
        Window w = GetComponentInParent<Window>();
        _windowSystem.BringToFront(w);
    }

    public void OnPointerDown(PointerEventData eventData) {
        Window w = GetComponentInParent<Window>();
        _windowSystem.BringToFront(w);
    }
}