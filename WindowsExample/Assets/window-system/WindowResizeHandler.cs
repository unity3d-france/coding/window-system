﻿// Copyright ©2019, Andre Plourde
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the “Software”), to deal in the Software without 
// restriction, including without limitation the rights to use, copy, modify, merge, publish, 
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom 
// the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or 
// substantial portions of the Software.
//
// The Software is provided “as is”, without warranty of any kind, express or implied, 
// including but not limited to the warranties of merchantability, fitness for a particular 
// purpose and noninfringement. In no event shall the authors or copyright holders X be 
// liable for any claim, damages or other liability, whether in an action of contract, tort or
// otherwise, arising from, out of or in connection with the software or the use or other 
// dealings in the Software.

using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// component a ajouter a un item permetant de changer la taille de la fenetre par drag.
/// </summary>
public class WindowResizeHandler : MonoBehaviour, IDragHandler, IPointerDownHandler {
    private RectTransform _rectTrans = null;
    private Window _window = null;
    private WindowSystem _windowSystem = null;

    private void Awake() {
        _rectTrans = GetComponentInParent<Window>().GetComponent<RectTransform>();
        _windowSystem = WindowSystem.Instance;
    }

    private void Start() {
        _window = GetComponentInParent<Window>();
    }

    public void OnDrag(PointerEventData data) {

        float currentWidth = _rectTrans.sizeDelta.x;
        float newWidth = currentWidth + data.delta.x;
        if (null != _window && newWidth < (float)_window.MinWidth) {
            newWidth = (float)_window.MinWidth;
        }
        float currentHeight = _rectTrans.sizeDelta.y;
        float newHeight = currentHeight - data.delta.y;
        if (null != _window && newHeight < (float)_window.MinHeight) {
            newHeight = (float)_window.MinHeight;
        }
        newWidth = Mathf.Max(newWidth, Mathf.Max(100,_window.MinWidth));
        newHeight = Mathf.Max(newHeight, Mathf.Max(100, _window.MinHeight));

        _rectTrans.sizeDelta = new Vector2(newWidth,
                                           newHeight);
    }

    public void OnPointerDown(PointerEventData eventData) {
        _windowSystem.BringToFront(_window);
    }
}